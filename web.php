<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('crud','SallesController@liste');
Route::post('crud/ajout','SallesController@inserer');
Route::get('crud/delete/{id}','SallesController@delete');
Route::get('crud/update/{id}','SallesController@modifier');
Route::post('crud/update/{id}','SallesController@update');
*/
//Route::get('crud/search','SallesController@search');
Route::get('crud/export-excel','SallesController@exportExcel');
Route::get('crud/export-pdf','SallesController@exportPdf');


Route::get('crud','SallesController@liste');
Route::post('crud/ajout','SallesController@inserer');
//Route::get('crud/delete/{id}','SallesController@delete');
//Route::get('crud/update/{id}','SallesController@modifier');
//Route::post('crud/update/{id}','SallesController@update');
Route::post('crud/tabledit','SallesController@action')->name('tabledit.action');

Route::get('/', function () {
    return view('welcome');
});

//Route::get('crud/export-pdf', function () {
  //  return view('salles.index');
//});
